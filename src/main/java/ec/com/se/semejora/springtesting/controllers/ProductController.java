package ec.com.se.semejora.springtesting.controllers;

import ec.com.se.semejora.springtesting.entities.Product;
import ec.com.se.semejora.springtesting.entities.Sample;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pazfernando on 10/20/17.
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    @PostMapping
    public ResponseEntity<Product> query(@Valid @RequestBody Product product) {
        return new ResponseEntity<>(new Product(), HttpStatus.CREATED);
    }
}
