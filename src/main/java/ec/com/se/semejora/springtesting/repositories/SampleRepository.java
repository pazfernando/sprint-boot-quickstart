package ec.com.se.semejora.springtesting.repositories;

import ec.com.se.semejora.springtesting.entities.Sample;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by pazfernando on 10/20/17.
 */
public interface SampleRepository extends CrudRepository<Sample, Long> {
}
