package ec.com.se.semejora.springtesting.controllers;

import ec.com.se.semejora.springtesting.entities.Sample;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pazfernando on 10/20/17.
 */
@RestController
@RequestMapping("/samples")
public class SampleController {
    @GetMapping
    public Sample get() {
        Sample sample = new Sample();
        sample.setId(1L);
        sample.setText("Text sample");
        return sample;
    }

    @GetMapping("/query")
    public List<Sample> query() {
        Sample sample = new Sample();
        sample.setId(1L);
        sample.setText("Text sample");
        Sample sample1 = new Sample();
        sample1.setId(2L);
        sample1.setText("Text sample TWO");
        List<Sample> samples = new ArrayList<>();
        samples.add(sample);
        samples.add(sample1);
        return samples;
    }
}
