package ec.com.se.semejora.springtesting.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by pazfernando on 10/20/17.
 */
@Configuration
@EnableJpaRepositories("ec.com.se.semejora.springtesting.repositories")
@EnableWebMvc
public class SpringTestingConfig {
}
