package ec.com.se.semejora.springtesting;

import ec.com.se.semejora.springtesting.entities.Sample;
import ec.com.se.semejora.springtesting.repositories.SampleRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by pazfernando on 10/20/17.
 */
@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class SampleRepositoryTest {
    @Autowired
    SampleRepository sampleRepository;

    Sample sample;

    @Before
    public void setUp() {
        sample = new Sample();
        sample.setText("Text sample");
    }

    @Test
    public void shouldSave() {
        sampleRepository.save(sample);
        Assert.assertNotNull(sample.getId());
    }
}
